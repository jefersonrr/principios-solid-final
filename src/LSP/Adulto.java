/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LSP;

/**
 *
 * @author USUARIO
 */
public class Adulto extends Persona {

    private String cc;
    private String tarjeta;

    public Adulto(String nombre, String apellidos, String cc, String tarjeta) {
        super(nombre, apellidos);
        this.cc = cc;
        this.tarjeta = tarjeta;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public void pagar() {

        System.out.println("mi Cc es " + getCc() + "pago con la tarjeta" + tarjeta);
    }

}
