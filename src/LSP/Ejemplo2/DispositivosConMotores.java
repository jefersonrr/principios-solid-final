/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LSP.Ejemplo2;

/**
 *
 * @author USUARIO
 */
public class DispositivosConMotores extends DispositivoDeTrasporte{
    
    Motor motor;

    public DispositivosConMotores(String nombre, Double velocidad, Motor motor) {
        super(nombre, velocidad);
        this.motor = motor;
    }

    
    void encenderMotor(){
    ///
    
    }

    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }
    
    
}
