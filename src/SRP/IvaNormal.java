/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRP;

/**
 *
 * @author LEONEL
 */
public class IvaNormal {
     private final  double PORCENTAJE_IVA_NORMAL = 0.19;

    public IvaNormal() {
    }
    
  
    public double CalcularIVA(double importe)
    {
        return importe * PORCENTAJE_IVA_NORMAL;
    }

    public double getPORCENTAJE_IVA_NORMAL() {
        return PORCENTAJE_IVA_NORMAL;
    }
    
}
