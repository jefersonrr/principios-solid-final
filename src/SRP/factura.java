/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRP;

/**
 *
 * @author LEONEL
 */
public class factura {
    
   
    public String Codigo;
    public double ImporteFactura;
    public double ImporteIVA;
    public double ImporteDeduccion;
    public double ImporteTotal;
    public double PorcentajeDeduccion;
 
 
    // Método que calcula el total de la factura
    public void CalcularTotal()
    {
        // Calculamos la deducción}
        Deduccion deduccion = new Deduccion(PorcentajeDeduccion);
        ImporteDeduccion = deduccion.CalcularDeduccion(ImporteFactura);
        // Calculamos el IVA
        IvaNormal iva = new IvaNormal();
        ImporteIVA = iva.CalcularIVA(ImporteFactura);
        // Calculamos el total
        ImporteTotal = (ImporteFactura - ImporteDeduccion) + ImporteIVA;
    }
    
   
}
