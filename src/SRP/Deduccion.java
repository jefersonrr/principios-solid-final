/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRP;

/**
 *
 * @author LEONEL
 */
public class Deduccion {
        private double m_PorcentajeDeduccion;

    public Deduccion(double porcentaje)
    {
        m_PorcentajeDeduccion = porcentaje;
    }
        
    public double CalcularDeduccion(double importe)
    {
        return (importe * m_PorcentajeDeduccion) / 100;
    }

    
}
