/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DIP;

/**
 *
 * @author USUARIO
 */
public class CestaDeCompra {
    
    private final AccionesBases accionesBases;
    private final MetodoDePago metodoPago;

    public CestaDeCompra(AccionesBases accionesBases, MetodoDePago metodoPago) {
        this.accionesBases = accionesBases;
        this.metodoPago = metodoPago;
    }
    
    
    
    
    public void comprar(Compra compra){


        accionesBases.guardar(compra);
        metodoPago.pagar(compra);
}
}
