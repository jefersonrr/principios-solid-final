/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OCP2;

/**
 *
 * @author LEONEL
 */

public class Square extends Polygon {
 
    int side;

    public Square(int side) {
        this.side = side;
    }
    
    @Override
    double calcularArea() {
        return Math.pow(side,2);
    }
   
}
